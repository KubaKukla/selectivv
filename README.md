# Selectivv full-stack test

## Getting started

Follow standard Laravel getting started guide

To migrate existing db
`php artisan migrate && php artisan db:seed`

## Tasks

You are building application for Evil Corp Marketing Deprtment to get insight about how campaign are running and to get live reports

**Rules:**
- Main goal of this test is not to do as much as possible, but do quality work instead
- Estimated time: 1h (it can be verified clearly when looking on your commit history) 
- Each tasks must be tested and placed in separate commit

**Tasks:**
1. Add event model. Event is a click or an impression that fall with campaign_id, latitude and longitude. Event is in relation with campaign (campaign have many events, event belongs to one campaign)
2. Add seeder for event that will generate 10k random events for 5 initially seeded campaigns
3. Add counting statistics for each campaign in view (number of click and impressions for each campaign). Adapt to exisiting view
4. Add showing 10 latest events with details
5. Add logic to form below latest events. It should add random click or impression based on choosen radio

Extra tasks (choose what you want if you have some time left)
- prove your testing skills (write unit/browser test)
- add async to application
- make it more user friendly (prove your ui designing skills and enchance look of the application)



## Copyright

Please do not redistribute this task

2017 © Selectivv Europe Sp. z.o.o., All Rights Reserved







