<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Campaign;

class EventController extends Controller
{
	/**
	 * Returns app view
	 * @return Response view
	 */
    public function index() {
    	$campaigns = Campaign::all();

    	return view('events.index')
    		->with('campaigns', $campaigns);
    }
}
