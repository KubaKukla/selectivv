<?php

use Illuminate\Database\Seeder;
use Faker as Faker;
use Carbon\Carbon;

class CampaignsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

    	foreach (range(1,5) as $index) {
	        DB::table('campaigns')->insert([
	            'name' => ucfirst($faker->word),
	            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
	        ]);
	    }
    }
}
